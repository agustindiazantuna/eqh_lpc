/*
 * task_init.c
 *
 *  Created on: 6 de nov. de 2016
 *      Author: agustin
 */


#include "header.h"


#if (USE_RTOS)


	void queue_init()
	{
		// Creo queue
		Queue_new_data = xQueueCreate(4, sizeof(uint8_t));
		Queue_efecto = xQueueCreate(4, sizeof(uint32_t));
		Queue_parametros = xQueueCreate(1, sizeof(param_t));
		Queue_init_ok = xQueueCreate(2, sizeof(uint8_t));
		Queue_display_ok = xQueueCreate(2, sizeof(uint8_t));
		// Creo semaphore


	}



	void task_init()
	{
		// Creo tareas
		#if (USE_SD)
			// TAREA SD
			xTaskCreate(vTaskSD, (signed char *) "vTaskSD",
						configMINIMAL_STACK_SIZE*4, NULL, (tskIDLE_PRIORITY + 5UL),
						(xTaskHandle *) NULL);
		#endif

		#if (USE_DISPLAY)
			xTaskCreate(vDisplayTask, (signed char *) "vDisplayTask",
						configMINIMAL_STACK_SIZE*2, NULL, (tskIDLE_PRIORITY + 4UL),
						(xTaskHandle *) NULL);
		#endif

		#if (USE_GPIO)
			// TAREA LED
			xTaskCreate(vTaskLED_alive, (signed char *) "vTaskLED_alive",
						configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
						(xTaskHandle *) NULL);

			#if(USE_BOTONES)
				// TAREA CONTROL
				xTaskCreate(vControlTask, (signed char *) "vControlTask",
							configMINIMAL_STACK_SIZE*2, NULL, (tskIDLE_PRIORITY + 2UL),
							(xTaskHandle *) NULL);
			#endif

		#endif

		#if (USE_ADC && USE_DAC)
			// TAREA SIGNAL_PROCESSING
			xTaskCreate(vTaskSIGNAL_PROC, (signed char *) "vTaskSIGNAL_PROC",
						configMINIMAL_STACK_SIZE*8, NULL, (tskIDLE_PRIORITY + 3UL),
						(xTaskHandle *) NULL);
		#endif
	}


#endif

