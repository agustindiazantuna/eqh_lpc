/*
 * task_func.c
 *
 *  Created on: 6 de nov. de 2016
 *      Author: agustin
 */


#include "header.h"


//volatile uint8_t flag_efecto = 0;
//volatile uint8_t flag_volumen_down = 0;

#if (USE_RTOS)

	#if (USE_SD)

		void vTaskSD(void *pvParameters)
		{
			TM_ILI9341_Puts(10, 80, "Leyendo tarjeta SD...", &TM_Font_11x18, ILI9341_COLOR_BLUE, ILI9341_COLOR_CYAN);

			param_t parameters = {0};
			UINT nbytes;
			char str[30];
			char buffer_read_SD [30] = {0};
			char coeficiente_read_SD[5] = {0};
			int i = 0;
			int j = 0;

			FRESULT SD_flag;
			/* Give a work area to the default drive */

			SD_flag = f_mount(&fs, "", 0);

			/* If this fails, it means that the function could
			 * not register a file system object.
			 * Check whether the SD card is correctly connected */
			if ( SD_flag != FR_OK)
			{}

//			if (f_open(&fp, FILENAME1, FA_WRITE | FA_CREATE_ALWAYS) == FR_OK)
//			{
//				f_write(&fp, "PROFUNDIDAD:345\r\nDISTORSION:123\r\n", strlen("PROFUNDIDAD:345\r\nDISTORSION:123\r\n"), &nbytes);		// NO DEJAR ESPACIOS ENTRE VALORES Y  LOS DOS PUNTOS ":"
//				f_close(&fp);
//			}

			/////////////////////////////////////////////////////////////////////////////////////////////////////////
			// ECO_IIR
			// eco_iir(dma_memory_adc_BUFF, dma_memory_dac_BUFF, DAC_DMA_CANT_MUESTRAS, 0, 16);
			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			if( f_open(&fp, "eco_iir.txt" , FA_READ | FA_OPEN_EXISTING) == FR_OK)
			{
				f_read(&fp, buffer_read_SD, strlen("DELAY:") + 2, &nbytes);		// SOLO HASTA 3 NUMEROS EN PROFUNDIDAD O DISTORSION, CUALQUIER COSA CAMBIAR EL 3 POR UN VALOR.
				f_close(&fp);
			}

			for(i=0; buffer_read_SD[i]!=':';i++)
			{}
			i++;

			for( j=0 ; buffer_read_SD[i]!='\0';j++)									// Parseando los datos
			{
				coeficiente_read_SD[j]=buffer_read_SD[i];
				i++;
			}

			parameters.eco_iir_delay = atoi(coeficiente_read_SD);

			for(j=0 ; j<5 ; j++)
				coeficiente_read_SD[j] = 0;
			for(j=0 ; j<30 ; j++)
				buffer_read_SD[j] = 0;

			/////////////////////////////////////////////////////////////////////////////////////////////////////////
			// OVERDRIVE
			// overdrive(dma_memory_adc_BUFF, dma_memory_dac_BUFF, DAC_DMA_CANT_MUESTRAS, 2);
			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			if( f_open(&fp, "over.txt" , FA_READ | FA_OPEN_EXISTING) == FR_OK)
			{
				f_read(&fp, buffer_read_SD, strlen("DISTORSION:") + 2, &nbytes);		// SOLO HASTA 3 NUMEROS EN PROFUNDIDAD O DISTORSION, CUALQUIER COSA CAMBIAR EL 3 POR UN VALOR.
				f_close(&fp);
			}

			for(i=0; buffer_read_SD[i]!=':';i++)
			{}
			i++;

			for( j=0 ; buffer_read_SD[i]!='\0';j++)									// Parseando los datos
			{
				coeficiente_read_SD[j]=buffer_read_SD[i];
				i++;
			}

			parameters.overdrive_distorsion = atoi(coeficiente_read_SD);

			for(j=0 ; j<5 ; j++)
				coeficiente_read_SD[j] = 0;
			for(j=0 ; j<30 ; j++)
				buffer_read_SD[j] = 0;

			/////////////////////////////////////////////////////////////////////////////////////////////////////////
			// REVERB
			// reverb(dma_memory_adc_BUFF, dma_memory_dac_BUFF, DAC_DMA_CANT_MUESTRAS, 0, 4);
			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			if( f_open(&fp, "reverb.txt" , FA_READ | FA_OPEN_EXISTING) == FR_OK)
			{
				f_read(&fp, buffer_read_SD, strlen("densidad:") + 2, &nbytes);		// SOLO HASTA 3 NUMEROS EN PROFUNDIDAD O DISTORSION, CUALQUIER COSA CAMBIAR EL 3 POR UN VALOR.
				f_close(&fp);
			}

			for(i=0; buffer_read_SD[i]!=':';i++)
			{}
			i++;

			for( j=0 ; buffer_read_SD[i]!='\0';j++)									// Parseando los datos
			{
				coeficiente_read_SD[j]=buffer_read_SD[i];
				i++;
			}

			parameters.reverb_densidad = atoi(coeficiente_read_SD);

			for(j=0 ; j<5 ; j++)
				coeficiente_read_SD[j] = 0;
			for(j=0 ; j<30 ; j++)
				buffer_read_SD[j] = 0;

			/////////////////////////////////////////////////////////////////////////////////////////////////////////
			// FLANGER
			// flanger(dma_memory_adc_BUFF, dma_memory_dac_BUFF, DAC_DMA_CANT_MUESTRAS, 0, 0, 4);
			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			if( f_open(&fp, "flanger.txt" , FA_READ | FA_OPEN_EXISTING) == FR_OK)
			{
				f_read(&fp, buffer_read_SD, strlen("delay:") + 2, &nbytes);		// SOLO HASTA 3 NUMEROS EN PROFUNDIDAD O DISTORSION, CUALQUIER COSA CAMBIAR EL 3 POR UN VALOR.
				f_close(&fp);
			}

			for(i=0; buffer_read_SD[i]!=':';i++)
			{}
			i++;

			for( j=0 ; buffer_read_SD[i]!='\0';j++)									// Parseando los datos
			{
				coeficiente_read_SD[j]=buffer_read_SD[i];
				i++;
			}

			parameters.flanger_delay = atoi(coeficiente_read_SD);

			for(j=0 ; j<5 ; j++)
				coeficiente_read_SD[j] = 0;
			for(j=0 ; j<30 ; j++)
				buffer_read_SD[j] = 0;

			// prueba
			if(parameters.overdrive_distorsion == 0)
				sprintf(str, "overdrive dist = ERROR");
			else
				sprintf(str, "overdrive dist = %d", parameters.overdrive_distorsion);
			TM_ILI9341_Puts( 20, 125, str, &TM_Font_11x18, ILI9341_COLOR_BLUE, ILI9341_COLOR_CYAN);

			for(j=0 ; j<30 ; j++)
				str[j] = 0;

			if(parameters.flanger_delay == 0)
				sprintf(str, "flanger delay = ERROR");
			else
				sprintf(str, "flanger delay = %d", parameters.flanger_delay);
			TM_ILI9341_Puts( 20, 150, str, &TM_Font_11x18, ILI9341_COLOR_BLUE, ILI9341_COLOR_CYAN);

			for(j=0 ; j<30 ; j++)
				str[j] = 0;

			if(parameters.eco_iir_delay == 0)
				sprintf(str, "eco iir delay = ERROR");
			else
				sprintf(str, "eco iir delay = %d", parameters.eco_iir_delay);
			TM_ILI9341_Puts( 20, 175, str, &TM_Font_11x18, ILI9341_COLOR_BLUE, ILI9341_COLOR_CYAN);

			for(j=0 ; j<30 ; j++)
				str[j] = 0;

			if(parameters.reverb_densidad == 0)
				sprintf(str, "reverb densidad = ERROR");
			else
				sprintf(str, "reverb densidad = %d", parameters.reverb_densidad);
			TM_ILI9341_Puts( 20, 200, str, &TM_Font_11x18, ILI9341_COLOR_BLUE, ILI9341_COLOR_CYAN);

			xQueueSend(Queue_parametros, &parameters, portMAX_DELAY);

			while(1)
			{
				vTaskDelay(1000/portTICK_RATE_MS);	// Por si hay que hacer algo con la SD

				vTaskDelete(NULL);
			}
		}

	#endif

	#if(USE_GPIO)

		// LED ALIVE
		void vTaskLED_alive(void *pvParameters) {

			while (1) {
				vTaskDelay( 1000 / portTICK_RATE_MS );
				Chip_GPIO_SetPinToggle(LPC_GPIO, LED_Alive);
			}
		}


		#if (USE_BOTONES)

			// Debouncer
			void vControlTask(void *pvParameters) {

				uint32_t EstadoDebounce = WAIT_1, tecla_actual = 0, tecla_anterior = 0;
				int MenuActual = 0, MenuAnterior = 0;
				uint8_t estado_init = 0;

				//SW1 baja y SW4 sube en el menú

				while (1)
				{
					switch(EstadoDebounce)
					{
						case WAIT_0:
							if( Chip_GPIO_GetPinState( LPC_GPIO , SW1 ) == 0 )
									tecla_actual=1;
							if( Chip_GPIO_GetPinState( LPC_GPIO , SW2 ) == 0 )
									tecla_actual=2;
							if( Chip_GPIO_GetPinState( LPC_GPIO , SW3 ) == 0 )
									tecla_actual=3;
							if( Chip_GPIO_GetPinState( LPC_GPIO , SW4 ) == 0 )
									tecla_actual=4;

							if( tecla_actual != 0)
								EstadoDebounce = VALIDATE_0;

							tecla_anterior = tecla_actual;
							break;
						case VALIDATE_0:
							vTaskDelay(50/portTICK_RATE_MS);

							if( Chip_GPIO_GetPinState( LPC_GPIO , SW1 ) == 0 )
									tecla_actual=1;
							if( Chip_GPIO_GetPinState( LPC_GPIO , SW2 ) == 0 )
									tecla_actual=2;
							if( Chip_GPIO_GetPinState( LPC_GPIO , SW3 ) == 0 )
									tecla_actual=3;
							if( Chip_GPIO_GetPinState( LPC_GPIO , SW4 ) == 0 )
									tecla_actual=4;

							if( tecla_anterior == tecla_actual)
								EstadoDebounce = WAIT_1;
							else
								EstadoDebounce = WAIT_0;

							tecla_actual = 0;
							break;
						case WAIT_1:
							if( Chip_GPIO_GetPinState( LPC_GPIO , SW1 ) == 1 )
									tecla_actual=1;
							if( Chip_GPIO_GetPinState( LPC_GPIO , SW2 ) == 1 )
									tecla_actual=2;
							if( Chip_GPIO_GetPinState( LPC_GPIO , SW3 ) == 1 )
									tecla_actual=3;
							if( Chip_GPIO_GetPinState( LPC_GPIO , SW4 ) == 1 )
									tecla_actual=4;

							if( tecla_actual != 0 )
								EstadoDebounce = VALIDATE_1;

							tecla_actual = 0;
							break;
						case VALIDATE_1:
							vTaskDelay(50/portTICK_RATE_MS);

							if( Chip_GPIO_GetPinState( LPC_GPIO , SW1 ) == 0 )
							{
								tecla_actual = 1;

								MenuActual--;
								if( MenuActual == -1 )
									MenuActual = (CANT_EFECTOS -1);
							}

							if( Chip_GPIO_GetPinState( LPC_GPIO , SW2 ) == 0 )
								tecla_actual = 2;

							if( Chip_GPIO_GetPinState( LPC_GPIO , SW3 ) == 0 )
								tecla_actual=3;

							if( Chip_GPIO_GetPinState( LPC_GPIO , SW4 ) == 0 )
							{
								tecla_actual=4;

								MenuActual++;
								if(MenuActual == (CANT_EFECTOS) )
									MenuActual=0;
							}

							if( 0 != tecla_actual )
								EstadoDebounce = WAIT_0;
							else
								EstadoDebounce = WAIT_1;

							tecla_actual = 0;
							break;
						default:
							break;
					}

					#if(USE_DISPLAY)

					xQueueReceive(Queue_display_ok, &estado_init, 0);

					if(estado_init == 1)
					{
						if( MenuActual != MenuAnterior )
						{
							xQueueSend(Queue_efecto, &MenuActual, portMAX_DELAY);

							switch( MenuActual )
							{
								case LOOPBACK:
									// Todos circulos azules, sin seleccion
									TM_ILI9341_DrawFilledCircle(30,85,7,ILI9341_COLOR_RED);
									TM_ILI9341_DrawFilledCircle(30,110,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,135,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,160,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,185,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,210,7,ILI9341_COLOR_BLUE);
									break;
								case DISTORSION:
									// Seleccion de Efecto 1 en pantalla
									TM_ILI9341_DrawFilledCircle(30,85,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,110,7,ILI9341_COLOR_RED);
									TM_ILI9341_DrawFilledCircle(30,135,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,160,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,185,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,210,7,ILI9341_COLOR_BLUE);
									break;
								case OVERDRIVE:
									// Seleccion de efectos 2 en pantalla
									TM_ILI9341_DrawFilledCircle(30,85,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,110,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,135,7,ILI9341_COLOR_RED);
									TM_ILI9341_DrawFilledCircle(30,160,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,185,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,210,7,ILI9341_COLOR_BLUE);
									break;
								case ECOIIR:
									// Seleccion de efectos 3 en pantalla
									TM_ILI9341_DrawFilledCircle(30,85,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,110,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,135,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,160,7,ILI9341_COLOR_RED);
									TM_ILI9341_DrawFilledCircle(30,185,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,210,7,ILI9341_COLOR_BLUE);
									break;
								case REVERB:
									// Seleccion de efectos 4 en pantalla
									TM_ILI9341_DrawFilledCircle(30,85,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,110,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,135,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,160,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,185,7,ILI9341_COLOR_RED);
									TM_ILI9341_DrawFilledCircle(30,210,7,ILI9341_COLOR_BLUE);
									break;
								case FLANGER:
									// Seleccion de efectos 4 en pantalla
									TM_ILI9341_DrawFilledCircle(30,85,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,110,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,135,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,160,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,185,7,ILI9341_COLOR_BLUE);
									TM_ILI9341_DrawFilledCircle(30,210,7,ILI9341_COLOR_RED);
									break;
								default:
									break;
							}
						}
						MenuAnterior = MenuActual;
					}
					#endif
				}
			}

		#endif
	#endif

	#if (USE_ADC && USE_DAC)

		// SIGNAL PROCESSING
		void vTaskSIGNAL_PROC(void *pvParameters) {

			uint32_t i = 0;
			uint8_t buffer_state = 0;

			param_t parameters = {0};

			uint32_t efecto = 0;

			uint32_t datos_sd = 0;

			while (1) {

				#if (USE_DSP_IRQ)
					vTaskDelay(1);
				#else
					if(xQueueReceive(Queue_parametros, &parameters, 0) == pdTRUE)
					{
						if(parameters.eco_iir_delay != 0 && parameters.flanger_delay != 0 && parameters.overdrive_distorsion != 0 && parameters.reverb_densidad != 0)
						{
							datos_sd = 1;
							xQueueSend(Queue_init_ok, &datos_sd, portMAX_DELAY);
						}
						else
							xQueueSend(Queue_init_ok, &datos_sd, 0);

					}

					xQueueReceive(Queue_efecto, &efecto, 0);

					if(xQueueReceive(Queue_new_data, &buffer_state, portMAX_DELAY) == pdTRUE)
					{
						if(datos_sd)
						{
							switch(efecto)
							{
								case DISTORSION:
									distorsion(dma_memory_adc_BUFF, dma_memory_dac_BUFF, DAC_DMA_CANT_MUESTRAS, 0);
									break;
								case OVERDRIVE:
									overdrive(dma_memory_adc_BUFF, dma_memory_dac_BUFF, DAC_DMA_CANT_MUESTRAS, parameters.overdrive_distorsion);
									break;
								case ECOIIR:
									eco_iir(dma_memory_adc_BUFF, dma_memory_dac_BUFF, DAC_DMA_CANT_MUESTRAS, 0, parameters.eco_iir_delay);
//									eco_fir(dma_memory_adc_BUFF, dma_memory_dac_BUFF, DAC_DMA_CANT_MUESTRAS, 0, 0);
									break;
								case REVERB:
									reverb(dma_memory_adc_BUFF, dma_memory_dac_BUFF, DAC_DMA_CANT_MUESTRAS, 0, parameters.reverb_densidad);
									break;
								case FLANGER:
									flanger(dma_memory_adc_BUFF, dma_memory_dac_BUFF, DAC_DMA_CANT_MUESTRAS, 0, 0, parameters.flanger_delay);
									break;
								default:
									for(i = 0; i < DAC_DMA_CANT_MUESTRAS; i++)
										dma_memory_dac_BUFF[i] = dma_memory_adc_BUFF[i];
									break;
							}
						}

						// Transmito el BUFFER
						Chip_GPDMA_SGTransfer(LPC_GPDMA, canal_dac,
												&DMA_descriptor_DAC_BUFFER,
												GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA);

					}
				#endif
			}
		}

	#endif


	#if (USE_DISPLAY)

		void vDisplayTask(void *pvParameters)
		{
			uint8_t estado_init = 0;

			vTaskDelay(5000/ portTICK_RATE_MS);

			xQueueReceive(Queue_init_ok, &estado_init, portMAX_DELAY);

			xQueueSend(Queue_display_ok, &estado_init, 0);

			if(estado_init == 0)
			{
				TM_ILI9341_Fill(ILI9341_COLOR_RED);
				TM_ILI9341_DrawRectangle(60, 90, 260, 150, ILI9341_COLOR_BLACK);
				TM_ILI9341_Puts(70, 110, "ERROR lectura SD", &TM_Font_11x18,  ILI9341_COLOR_BLACK, ILI9341_COLOR_RED);
			}
			else
			{
				TM_ILI9341_Fill(ILI9341_COLOR_CYAN);

				TM_ILI9341_Rotate(TM_ILI9341_Orientation_Landscape_2);

				//TM_ILI9341_DrawRectangle(WINDOWHEIGTHX1,WINDOWWIDTHX1,WINDOWHEIGTHX2,WINDOWWIDTHX2,ILI9341_COLOR_WHITE);

				TM_ILI9341_Puts(40, 0, "Procesamiento digital", &TM_Font_11x18,  ILI9341_COLOR_GREEN2, ILI9341_COLOR_CYAN);
				TM_ILI9341_Puts(110, 25, "de audio", &TM_Font_11x18,  ILI9341_COLOR_GREEN2, ILI9341_COLOR_CYAN);
				TM_ILI9341_Puts(5, 50, "Efectos:", &TM_Font_11x18,  ILI9341_COLOR_BLUE, ILI9341_COLOR_CYAN);

				TM_ILI9341_DrawFilledCircle(30,85,7,ILI9341_COLOR_RED);
				TM_ILI9341_Puts( 50, 75, "Loopback", &TM_Font_11x18,  ILI9341_COLOR_GRAY, ILI9341_COLOR_CYAN);
				TM_ILI9341_DrawFilledCircle(30,110,7,ILI9341_COLOR_BLUE);
				TM_ILI9341_Puts( 50, 100, "Distorsion", &TM_Font_11x18,  ILI9341_COLOR_GRAY, ILI9341_COLOR_CYAN);
				TM_ILI9341_DrawFilledCircle(30,135,7,ILI9341_COLOR_BLUE);
				TM_ILI9341_Puts( 50, 125, "Overdrive", &TM_Font_11x18,  ILI9341_COLOR_GRAY, ILI9341_COLOR_CYAN);
				TM_ILI9341_DrawFilledCircle(30,160,7,ILI9341_COLOR_BLUE);
				TM_ILI9341_Puts( 50, 150, "Eco IIR", &TM_Font_11x18,  ILI9341_COLOR_GRAY, ILI9341_COLOR_CYAN);
				TM_ILI9341_DrawFilledCircle(30,185,7,ILI9341_COLOR_BLUE);
				TM_ILI9341_Puts	(50, 175, "Reverb", &TM_Font_11x18,  ILI9341_COLOR_GRAY, ILI9341_COLOR_CYAN);
				TM_ILI9341_DrawFilledCircle(30,210,7,ILI9341_COLOR_BLUE);
				TM_ILI9341_Puts	(50, 200, "Flanger", &TM_Font_11x18,  ILI9341_COLOR_GRAY, ILI9341_COLOR_CYAN);
			}

			while(1)
			{
				vTaskDelay(1000/portTICK_RATE_MS);

				vTaskDelete(NULL);
			}
		}

	#endif


#endif




