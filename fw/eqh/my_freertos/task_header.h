/*
 * task_header.h
 *
 *  Created on: 6 de nov. de 2016
 *      Author: agustin
 */

#ifndef TASK_HEADER_H_
#define TASK_HEADER_H_



#include <header.h>



// ********* FUNCIONES ********** //
	#if (USE_RTOS)
		void queue_init();
		void task_init();
	#endif
// ****************************** //


// ***************** DEFINES ***************** //

	#define FILENAME1 "overdrive.txt"
	#define FILENAME2 "eco_iir.txt"
	#define FILENAME3 "reverb.txt"
	#define FILENAME4 "flanger.txt"

	#define CANT_EFECTOS 	6

	#define LOOPBACK		0
	#define DISTORSION		1
	#define OVERDRIVE		2
	#define ECOIIR			3
	#define REVERB			4
	#define FLANGER			5

// ******************************************* //


// ************* VARIABLES GLOBALES ************* //
	#if (USE_RTOS)

		// Queue
		extern volatile xQueueHandle Queue_new_data;
		extern volatile xQueueHandle Queue_efecto;
		extern volatile xQueueHandle Queue_parametros;
		extern volatile xQueueHandle Queue_init_ok;
		extern volatile xQueueHandle Queue_display_ok;

		// Semaphore


		#if(USE_SD)
				extern volatile FATFS fs;           /**< FatFs work area needed for each volume */
				extern volatile FIL fp;             /**< File object needed for each open file */
		#endif

	#endif


// ********************************************** //


// ************* ESTRUCTURAS ************* //

	// LCD options: Used private
	typedef struct {
		uint32_t overdrive_distorsion;
		uint32_t eco_iir_delay;
		uint32_t reverb_densidad;
		uint32_t flanger_delay;
	} param_t;

// ********************************************** //



// ************* INICIALIZACION ************* //
		#include "FreeRTOS.h"
		#include "task.h"
		#include "queue.h"
		#include "semphr.h"

		#include "task_func.h"

// ****************************************** //



#endif /* TASK_HEADER_H_ */
