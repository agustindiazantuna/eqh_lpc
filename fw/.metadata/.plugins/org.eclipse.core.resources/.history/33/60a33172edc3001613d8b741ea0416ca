/*
 * efectos_func.c
 *
 *  Created on: 8 de dic. de 2016
 *      Author: agustin
 */


#include "header.h"


#if (USE_EFECTOS)

	void distorsion(q31_t *in, q31_t *out, uint16_t length, q31_t dist)
	{
		for(int i=0; i<length; i++)
		{
			if(in[i] > (q31_t) 0x10000000)
			{
				out[i] = (q31_t) 0x10000000;
			}
			else if(in[i] < (q31_t) 0xA0000000)
			{
				out[i] = (q31_t) 0xA0000000;
			}
			else
			{
				out[i] = in[i];
			}
				out[i] *= 3;
		}
	}

	void overdrive(q31_t *in, q31_t *out, uint16_t length, q31_t dist)
	{
		for(int i=0; i<length; i++)
		{
			in[i] *= dist;

			if(in[i] > 0)
			{
				arm_sqrt_q31(in[i], &out[i]);
			}
			else
			{
				arm_abs_q31(&in[i], &out[i], 1);
				arm_sqrt_q31(out[i], &out[i]);
				out[i] *= -1;
			}
		}
	}

	volatile static q31_t delay_line[DELAY_LENGTH] __attribute__ ((section (".bss.$RamAHB32"))) = {0} ;

	void eco_iir(q31_t *in, q31_t *out, uint16_t length, q31_t profundidad, uint8_t delay)
	{
		static uint16_t delay_in_idx = 0;   // Indice para agregar muestra al delay_line
		static uint16_t delay_out_idx = 1;  // Indice para sumar al eco una muestra de la delay_line
		static uint8_t densidad_cont = 0;   // Sirve para agregar al delay_line una muestra de cada 'densidad' elementos (suma al eco 'densidad' veces la misma muestra)

		q31_t aux = 0;

		//prueba
		//profundidad = 0x70000000;
		q31_t at = 0x70000000;
		delay = 16;
		////////

		for(int i=0; i<length; i++)
		{
			aux = in[i] >> 8;

			out[i] = 0; // Eco IIR

			//eco = in - (profundidad*delay_line[delay_out_idx]); // Calcula el eco sumando una muestra anterior guardada en la delay_line
//			out[i] = (at*in[i]) +(at*(profundidad*delay_line[delay_out_idx])); // Calcula el eco sumando una muestra anterior guardada en la delay_line
//			out[i] = (in[i] + (profundidad*delay_line[delay_out_idx])); // Calcula el eco sumando una muestra anterior guardada en la delay_line
			out[i] = (aux + (delay_line[delay_out_idx] >> 1)); // Calcula el eco sumando una muestra anterior guardada en la delay_line



				// Una de cada 'delay' muestras, agrega el eco a la delay_line
			densidad_cont++;
			densidad_cont %= delay;

			if(!densidad_cont)
			{
				delay_line[delay_in_idx] = out[i];

				delay_in_idx++;
				delay_in_idx %= DELAY_LENGTH;
				delay_out_idx++;
				delay_out_idx %= DELAY_LENGTH;
			}

			out[i] = out[i] << 7;

			//prueba
//			out[i] = delay_line[delay_out_idx];
			////////
		}
	}


//	void eco_fir(q31_t *in, q31_t *out, uint16_t length, q31_t profundidad, uint8_t delay)
//	{
//		static uint16_t delay_in_idx = 0;   // Indice para agregar muestra al delay_line
//		static uint16_t delay_out_idx = 2;  // Indice para sumar al eco una muestra de la delay_line
//		static uint16_t densidad_cont = 0;   // Sirve para agregar al delay_line una muestra de cada 'densidad' elementos (suma al eco 'densidad' veces la misma muestra)
//
//		//prueba
//		profundidad = 0x70000000;
//		q31_t at = 0x40000000;
//		delay = 1;
//		////////
//
//		for(int i=0; i<length; i++)
//		{
//			out[i] = 0; // Eco IIR
//
//			//eco = in - (profundidad*delay_line[delay_out_idx]); // Calcula el eco sumando una muestra anterior guardada en la delay_line
////			out[i] = (at*in[i]) +(at*(profundidad*delay_line[delay_out_idx])); // Calcula el eco sumando una muestra anterior guardada en la delay_line
////			out[i] = (in[i] + (profundidad*delay_line[delay_out_idx])); // Calcula el eco sumando una muestra anterior guardada en la delay_line
//			out[i] = (in[i] + (profundidad*delay_line[delay_out_idx])); // Calcula el eco sumando una muestra anterior guardada en la delay_line
//
//			// Una de cada 'delay' muestras, agrega el eco a la delay_line
//			densidad_cont++;
//			densidad_cont %= 7;
//
//			if(!densidad_cont)
//			{
//				delay_line[delay_in_idx] = in[i];
//
//				delay_in_idx++;
//				delay_in_idx %= DELAY_LENGTH;
//				delay_out_idx++;
//				delay_out_idx %= DELAY_LENGTH;
//			}
//
//			//prueba
////			out[i] = delay_line[delay_out_idx];
//			////////
//		}
//	}
//


	void reverb(q31_t *in, q31_t *out, uint16_t length, q31_t profundidad, uint8_t densidad)
	{
		static uint16_t delay_in_idx = DELAY_LENGTH;                   // Indice para agregar muestra al delay_line
		static uint16_t delay_out_idx[CANT_REVERB] = {DELAY_LENGTH/3, DELAY_LENGTH/2};   // Indice para sumar al eco una muestra de la delay_line
		static uint8_t densidad_cont = 0;                   // Sirve para agregar al delay_line una muestra de cada 'densidad' elementos (suma al eco 'densidad' veces la misma muestra)

		uint16_t aux = 0;

		for(int i=0; i<length; i++)
		{
			out[i] = 0; // Reverb

			out[i] = in[i] >> 8;

			for(int i=0; i<CANT_REVERB; i++)
			{
				aux = delay_out_idx[i];
				out[i] += (delay_line[aux] >> i); // Calcula el eco sumando una muestra anterior guardada en la delay_line
			}

			// Una de cada 'densidad' muestras, agrega el eco a la delay_line
			densidad_cont++;
			densidad_cont %= densidad;

			if(!densidad_cont)
			{
				delay_in_idx++;
				delay_in_idx %= DELAY_LENGTH;

				for(int i=0; i<CANT_REVERB; i++)
				{
					delay_out_idx[i]++;
					delay_out_idx[i] %= DELAY_LENGTH;
				}

				delay_line[delay_in_idx] = out[i];
			}

			out[i] = out[i] << 6;
		}
	}









	#define LFO_LENGTH_VEL	20
	#define LFO_LENGTH		320
//	volatile static q31_t coseno[COS_LENGTH] __attribute__ ((section (".bss.$RAM2"))) = { 0x00000000,0x02835b5a,0x05067735,0x07891419,0x0a0af29a,0x0c8bd35f,0x0f0b7728,0x11899ed4,
//			 0x14060b68,0x16807e15,0x18f8b83d,0x1b6e7b7a,0x1de189a6,0x2051a4dd,0x22be8f88,0x25280c5e,0x278dde6f,0x29efc926,0x2c4d9051,0x2ea6f827,0x30fbc54e,0x334bbcdf,0x3596a46d,
//			 0x37dc420d,0x3a1c5c57,0x3c56ba71,0x3e8b240f,0x40b9617d,0x42e13ba4,0x45027c0d,0x471cece7,0x4930590f,0x4b3c8c12,0x4d415234,0x4f3e7875,0x5133cc95,0x53211d19,0x55063951,
//			 0x56e2f15e,0x58b71632,0x5a82799a,0x5c44ee40,0x5dfe47ae,0x5fae5a55,0x6154fb91,0x62f201ad,0x648543e4,0x660e9a6b,0x678dde6f,0x6902ea1d,0x6a6d98a5,0x6bcdc63a,0x6d23501b,
//			 0x6e6e1493,0x6fadf2fd,0x70e2cbc7,0x720c8075,0x732af3a8,0x743e0918,0x7545a5a1,0x7641af3d,0x77320d0e,0x7816a759,0x78ef6790,0x79bc384e,0x7a7d055c,0x7b31bbb3,0x7bda497d,
//			 0x7c769e18,0x7d06aa17,0x7d8a5f40,0x7e01b096,0x7e6c9251,0x7ecaf9e5,0x7f1cde01,0x7f623690,0x7f9afcb9,0x7fc72ae3,0x7fe6bcb0,0x7ff9af05,0x7fffffff,0x7ff9af05,0x7fe6bcb0,
//			 0x7fc72ae3,0x7f9afcb9,0x7f623690,0x7f1cde01,0x7ecaf9e5,0x7e6c9251,0x7e01b096,0x7d8a5f40,0x7d06aa17,0x7c769e18,0x7bda497d,0x7b31bbb3,0x7a7d055c,0x79bc384e,0x78ef6790,
//			 0x7816a759,0x77320d0e,0x7641af3d,0x7545a5a1,0x743e0918,0x732af3a8,0x720c8075,0x70e2cbc7,0x6fadf2fd,0x6e6e1493,0x6d23501b,0x6bcdc63a,0x6a6d98a5,0x6902ea1d,0x678dde6f,
//			 0x660e9a6b,0x648543e4,0x62f201ad,0x6154fb91,0x5fae5a55,0x5dfe47ae,0x5c44ee40,0x5a82799a,0x58b71632,0x56e2f15e,0x55063951,0x53211d19,0x5133cc95,0x4f3e7875,0x4d415234,
//			 0x4b3c8c12,0x4930590f,0x471cece7,0x45027c0d,0x42e13ba4,0x40b9617d,0x3e8b240f,0x3c56ba71,0x3a1c5c57,0x37dc420d,0x3596a46d,0x334bbcdf,0x30fbc54e,0x2ea6f827,0x2c4d9051,
//			 0x29efc926,0x278dde6f,0x25280c5e,0x22be8f88,0x2051a4dd,0x1de189a6,0x1b6e7b7a,0x18f8b83d,0x16807e15,0x14060b68,0x11899ed4,0x0f0b7728,0x0c8bd35f,0x0a0af29a,0x07891419,
//			 0x05067735,0x02835b5a,0x00000001,0xfd7ca4a7,0xfaf988cc,0xf876ebe8,0xf5f50d67,0xf3742ca2,0xf0f488d9,0xee76612d,0xebf9f499,0xe97f81ec,0xe70747c4,0xe4918487,0xe21e765b,
//			 0xdfae5b24,0xdd417079,0xdad7f3a3,0xd8722192,0xd61036db,0xd3b26fb0,0xd15907da,0xcf043ab3,0xccb44322,0xca695b94,0xc823bdf4,0xc5e3a3aa,0xc3a94590,0xc174dbf2,0xbf469e84,
//			 0xbd1ec45d,0xbafd83f4,0xb8e3131a,0xb6cfa6f2,0xb4c373ef,0xb2beadcd,0xb0c1878c,0xaecc336c,0xacdee2e8,0xaaf9c6b0,0xa91d0ea3,0xa748e9cf,0xa57d8667,0xa3bb11c1,0xa201b853,
//			 0xa051a5ac,0x9eab0470,0x9d0dfe54,0x9b7abc1d,0x99f16596,0x98722192,0x96fd15e4,0x9592675c,0x943239c7,0x92dcafe6,0x9191eb6e,0x90520d04,0x8f1d343a,0x8df37f8c,0x8cd50c59,
//			 0x8bc1f6e9,0x8aba5a60,0x89be50c4,0x88cdf2f3,0x87e958a8,0x87109871,0x8643c7b3,0x8582faa5,0x84ce444e,0x8425b684,0x838961e9,0x82f955ea,0x8275a0c1,0x81fe4f6b,0x81936db0,
//			 0x8135061c,0x80e32200,0x809dc971,0x80650348,0x8038d51e,0x80194351,0x800650fc,0x80000000,0x800650fc,0x80194351,0x8038d51e,0x80650348,0x809dc971,0x80e32200,0x8135061c,
//			 0x81936db0,0x81fe4f6b,0x8275a0c1,0x82f955ea,0x838961e9,0x8425b684,0x84ce444e,0x8582faa5,0x8643c7b3,0x87109871,0x87e958a8,0x88cdf2f3,0x89be50c4,0x8aba5a60,0x8bc1f6e9,
//			 0x8cd50c59,0x8df37f8c,0x8f1d343a,0x90520d04,0x9191eb6e,0x92dcafe6,0x943239c7,0x9592675c,0x96fd15e4,0x98722192,0x99f16596,0x9b7abc1d,0x9d0dfe54,0x9eab0470,0xa051a5ac,
//			 0xa201b853,0xa3bb11c1,0xa57d8667,0xa748e9cf,0xa91d0ea3,0xaaf9c6b0,0xacdee2e8,0xaecc336c,0xb0c1878c,0xb2beadcd,0xb4c373ef,0xb6cfa6f2,0xb8e3131a,0xbafd83f4,0xbd1ec45d,
//			 0xbf469e84,0xc174dbf2,0xc3a94590,0xc5e3a3aa,0xc823bdf4,0xca695b94,0xccb44322,0xcf043ab3,0xd15907da,0xd3b26fb0,0xd61036db,0xd8722192,0xdad7f3a3,0xdd417079,0xdfae5b24,
//			 0xe21e765b,0xe4918487,0xe70747c4,0xe97f81ec,0xebf9f499,0xee76612d,0xf0f488d9,0xf3742ca2,0xf5f50d67,0xf876ebe8,0xfaf988cc,0xfd7ca4a7,} ;


	volatile static int lfo[LFO_LENGTH] = { 0,77,153,230,306,382,458,534,610,686,761,836,910,985,1059,1132,1205,1278,1350,1421,1492,
			1563,1633,1702,1771,1838,1906,1972,2038,2103,2167,2230,2292,2354,2414,2474,2533,2591,2647,2703,2758,2811,2864,2915,2966,3015,3063,3110,3155,3200,3243,3285,3325,3365,
			3403,3439,3475,3509,3542,3573,3603,3632,3659,3685,3709,3732,3754,3774,3792,3809,3825,3839,3852,3863,3873,3881,3888,3893,3897,3899,3900,3899,3897,3893,3888,3881,3873,
			3863,3852,3839,3825,3809,3792,3774,3754,3732,3709,3685,3659,3632,3603,3573,3542,3509,3475,3439,3403,3365,3325,3285,3243,3200,3155,3110,3063,3015,2966,2915,2864,2811,
			2758,2703,2647,2591,2533,2474,2414,2354,2292,2230,2167,2103,2038,1972,1906,1838,1771,1702,1633,1563,1492,1421,1350,1278,1205,1132,1059,985,910,836,761,686,610,534,458,
			382,306,230,153,77,0,-77,-153,-230,-306,-382,-458,-534,-610,-686,-761,-836,-910,-985,-1059,-1132,-1205,-1278,-1350,-1421,-1492,-1563,-1633,-1702,-1771,-1838,-1906,-1972,
			-2038,-2103,-2167,-2230,-2292,-2354,-2414,-2474,-2533,-2591,-2647,-2703,-2758,-2811,-2864,-2915,-2966,-3015,-3063,-3110,-3155,-3200,-3243,-3285,-3325,-3365,-3403,-3439,
			-3475,-3509,-3542,-3573,-3603,-3632,-3659,-3685,-3709,-3732,-3754,-3774,-3792,-3809,-3825,-3839,-3852,-3863,-3873,-3881,-3888,-3893,-3897,-3899,-3900,-3899,-3897,-3893,
			-3888,-3881,-3873,-3863,-3852,-3839,-3825,-3809,-3792,-3774,-3754,-3732,-3709,-3685,-3659,-3632,-3603,-3573,-3542,-3509,-3475,-3439,-3403,-3365,-3325,-3285,-3243,-3200,
			-3155,-3110,-3063,-3015,-2966,-2915,-2864,-2811,-2758,-2703,-2647,-2591,-2533,-2474,-2414,-2354,-2292,-2230,-2167,-2103,-2038,-1972,-1906,-1838,-1771,-1702,-1633,-1563,
			-1492,-1421,-1350,-1278,-1205,-1132,-1059,-985,-910,-836,-761,-686,-610,-534,-458,-382,-306,-230,-153,-77 };

	void flanger(q31_t *in, q31_t *out, uint16_t length, q31_t profundidad, uint16_t cos_f, uint8_t delay)
	{
		static uint16_t flanger_in_idx = 0;   // Indice para agregar muestra al delay_line
//		static uint16_t flanger_out_idx = 0;  // Indice para sumar al eco una muestra de la delay_line
//		static uint16_t cos_n = 0;          // numero de muestra de la funcion coseno

		static uint8_t densidad_cont = 0;   // Sirve para agregar al delay_line una muestra de cada 'densidad' elementos (suma al eco 'densidad' veces la misma muestra)

		static uint16_t lfo_idx = 0;		// Indice para recorrer el seno, aumenta de a 10 muestras procesadas
		static uint16_t lfo_idx_aux = 0;		// Indice para recorrer el seno, aumenta de a 10 muestras procesadas

		uint16_t flanger_idx;               // Para calcular el indice del delay_line segun una funcion coseno

		for(int i=0; i<length; i++)
		{
			if(! (i %= 10) )
				flanger_idx = DELAY_LENGTH/2 + lfo[lfo_idx];

			in[i] = in[i] >> 8;

			out[i] = (in[i] + (delay_line[flanger_idx] >> 1)); // Calcula el eco sumando una muestra anterior guardada en la delay_line

			lfo_idx_aux ++;
			lfo_idx_aux %= LFO_LENGTH_VEL;

			if(!lfo_idx_aux)
			{
				lfo_idx ++;
				lfo_idx %= LFO_LENGTH;
			}

			// Una de cada 'delay' muestras, agrega el eco a la delay_line
			densidad_cont++;
			densidad_cont %= delay;

			if(!densidad_cont)
			{
				delay_line[flanger_in_idx] = out[i];

				flanger_in_idx++;
				flanger_in_idx %= DELAY_LENGTH;
//				flanger_out_idx++;
//				flanger_out_idx %= DELAY_LENGTH;
			}

			out[i] = out[i] << 7;






//			cos_n++;
//			if( (2*PI*cos_f*TS - 1) == 0 )
//				cos_n = 0;
//
//			flanger_idx = ((DELAY_LENGTH/2)-1)*cos(2*PI*cos_f*TS*cos_n) - (DELAY_LENGTH/2);
//			flanger_idx = delay_out_idx - flanger_idx;
//
//			if(flanger_idx > DELAY_LENGTH)
//			{
//				flanger_idx = DELAY_LENGTH - (pow(2,16)-flanger_idx);
//			}
//
//			out[i] = in[i] + (profundidad*delay_line[flanger_idx]); // Calcula el eco sumando una muestra anterior guardada en la delay_line
//
//				// Una de cada 'densidad' muestras, agrega el eco a la delay_line
//			densidad_cont++;
//			densidad_cont %= densidad;
//
//			if(!densidad_cont)
//			{
//				delay_in_idx++;
//				delay_in_idx %= DELAY_LENGTH;
//				delay_out_idx++;
//				delay_out_idx %= DELAY_LENGTH;
//
//				delay_line[delay_in_idx] = out[i];
//			}
		}
	}



#endif

