# EQH #

## branch master ##

Ecualizador digital implementado en LPC1769

Realizado como proyecto de Técnicas Digitales 2
Cátedra correspondiente a la carrera Ingeniería Electrónica
Facultad Regional Buenos Aires
Universidad Tecnológica Nacional

Año 2016

### Contenidos ###
* LPC1769 basado en Cortex M3
* ADC: PCM1802 de Texas Instruments
* DAC: PCM1781 de Texas Instruments
* Comunicación I2S
* Manejo de tarjeta de memoria SD
* Display TFT con controlador ILI9341
* Efectos digitales sobre audio

### Contacto ###

* agustin.diazantuna@gmail.com
* Asunto: EQH LPC1769

## =================================================== ##

## los otros branch ##

Medidor de THD implementado en LPC1769

Realizado para la misma materia.

### Contacto ###

* audiodplab@frba.utn.edu.ar
* Asunto: Medidor de THD LPC1769